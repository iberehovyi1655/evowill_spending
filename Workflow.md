Install needed gems (for database, for tests).

**Tables structure:**
Categories: id, name.
Users: id, login (unique), password (hashed).
Spending: id, price (float), category_id, user_id, spent_at (date).

Create file structure.sql (create tables). 
Create file content.sql (insert basic date to tables).

We needs to keep user.id to know who is logged in.
We needs to give an opportunity to exit from app on any step. For example by "exit" command or "exit" menu item.
We needs to give an opportunity to reset any input if validation return false. 
We needs to give an opportunity to return to previous menu in logged in menus. 
We needs to give an opportunity to logout and return to main menu.

**Menu:**
Select menu item number:
1. Login
2. Create account

If user select item 1:
**Menu:**
Enter Login:
Enter Password:

If user select item 2:
**Menu:**
Enter Login:
Enter Password:

-------------------------------------------------------
**Menu:**
Select menu item number:
1. Add spending for categories
    1. Custom category
        Category name
        Price
        Select date (if empty use today)
        Select year (validate < 4 symbols)
        Select month number (validate not more 12)
        Select date number (validate <= count of the days in month)
    2. Category from list
        List of categories:
        1. Category name 1
        2. Category name 2
            Select category id
            Select price (only int or float. If string - show message "Please input only numbers." and reload input.) 
            Select date (if empty use today) 
                Select year (validate < 4 symbols)
                Select month number (validate not more 12)
                Select date number (validate <= count of the days in month)

2. Statistics:
    1. All categories statistic 
        1. Full statistic    
            Table in format: categories.name | SUM(categories.price) (group by name)
        2. Daily statistic    
            Table in format: categories.name | SUM(categories.price) (group by name) (filter by day)
        3. Months statistic  
            Table in format: categories.name | SUM(categories.price) (group by name) (filter by month)
        4. Yearly statistic
            Table in format: categories.name | SUM(categories.price) (group by name) (filter by year)
    2. Single category statistic
        1. Full statistic 
           1. Category name 1 
           2. Category name 2 
        2. Daily statistic    
           Select year (validate < 4 symbols)
           Select month number (validate not more 12)
           Select date number (validate <= count of the days in month)
        3. Monthly statistic
           Select year (validate < 4 symbols)
           Select month number (validate not more 12)
        4. Yearly statistic
           Select year (validate < 4 symbols)

3. Reset all statistics

