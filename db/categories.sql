CREATE table `categories`
(
    `id`   int         NOT NULL AUTO_INCREMENT,
    `name` varchar(50) NOT NULL UNIQUE,
    PRIMARY KEY (`id`)
);
