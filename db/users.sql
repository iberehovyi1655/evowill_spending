CREATE table `users`
(
    `id`       int          NOT NULL AUTO_INCREMENT,
    `login`    varchar(50)  NOT NULL UNIQUE,
    `password` varchar(255) NOT NULL,

    PRIMARY KEY (`id`)
);
