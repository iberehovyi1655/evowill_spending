CREATE table `spendings`
(
    `id`          int   NOT NULL AUTO_INCREMENT,
    `price`       float unsigned NOT NULL ,
    `category_id` int   NOT NULL,
    `user_id`     int   NOT NULL,
    `spent_at`    date  NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`category_id`) REFERENCES categories (`id`),
    FOREIGN KEY (`user_id`) REFERENCES users (`id`)
);
