module Database
  attr_reader :connection

  def create_structure
    create_structure_categories
    create_structure_users
    create_structure_spendings
  end

  def seeds
    categories_seeds
    users_seeds
  end

  private

  def db_connect
    if @connection
      @connection
    end
    require 'mysql'
    @db_host = "127.0.0.1"
    @db_user = "Ivan"
    @db_pass = "124"
    @db_name = "spending"
    @connection = Mysql.connect("mysql://#{@db_user}:#{@db_pass}@#{@db_host}:3306/#{@db_name}?charset=utf8mb4")
  end

  def create_structure_categories
    categories_stmt = @connection.prepare(File.read('db/categories.sql'))
    categories_stmt.execute
    puts "Categories table created."
  end

  def create_structure_users
    users_stmt = @connection.prepare(File.read('db/users.sql'))
    users_stmt.execute
    puts "Users table created."
  end

  def create_structure_spendings
    spendings_stmt = @connection.prepare(File.read('db/spendings.sql'))
    spendings_stmt.execute
    puts "Spendings table created."
  end

  def categories_seeds
    c_stmt = @connection.prepare(File.read('db/categories_seeds.sql'))
    c_stmt.execute
    puts "Successfully seeds into categories table"
  end

  def users_seeds
    u_stmt = @connection.prepare(File.read('db/users_seeds.sql'))
    u_stmt.execute
    puts "Successfully seeds into users table"
  end
end
