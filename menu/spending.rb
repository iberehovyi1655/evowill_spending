class Spending < Menu
  def initialize(user_id)
    require 'date'
    @user_id = user_id
    @menu = {
      "1" => { "text" => "1. Custom category\n", "method_name" => "custom_category" },
      "2" => { "text" => "2. Category from list.\n", "method_name" => "category_from_list" },
      "p" => { "text" => "p. Previous menu.", "method_name" => "previous_menu" },
      "x" => { "text" => "x. Exit.", "method_name" => "exit_app" }
    }
    db_connect
  end

  def menu(menu_hash = nil, menu_method_name = nil)
    puts "Spendings menu:"
    super
  end

  private

  def custom_category
    set_category_name
    save_category
    set_price
    set_date
    save_spending
  end

  def set_category_name
    puts "Category name:"
    @category_name = gets.chomp
  end

  def category_from_list
    show_categories
    set_category_id
    set_price
    set_date
    save_spending
  end

  def save_spending
    begin
      stmt = connection.prepare('INSERT INTO spendings (price, category_id, user_id, spent_at) VALUES (?, ?, ?, ?)')
      stmt.execute(@price, @category_id, @user_id, @date)
    rescue Mysql::ServerError::TruncatedWrongValueForField
      puts "Incorrectly entered value. Try again"
      category_from_list
    end
    successfully
  end

  def save_category
    begin
      stmt = connection.prepare('INSERT INTO categories (name) VALUES (?)')
      @category_id = stmt.execute(@category_name).insert_id
    rescue Mysql::ServerError::DupEntry
      puts "This category already exist. Please enter another name or choose from existing categories."
      menu
    end
  end

  def set_price
    puts "Price:"
    @price = gets.chomp.to_f
    if @price <= 0
      puts "Enter a number greater than 0"
      set_price
    end
  end

  def set_date
    puts "Fill the date fields or if you want to use current - leave the field blank"
    begin
      set_year
      set_month
      set_day_of_month
      @date = Date.parse("#{@year}-#{@month}-#{@day_of_month}").to_s
    rescue ArgumentError
      puts "Please insert valid date."
      menu
    end
  end

  def set_year
    puts "Year:"
    @year = gets.chomp
    if @year.empty?
      @year = Date.today.year.to_s
    end
  end

  def set_month
    puts "Month:"
    @month = gets.chomp
    if @month.empty?
      @month = Date.today.month.to_s
    end
  end

  def set_day_of_month
    puts "Day:"
    @day_of_month = gets.chomp
    if @day_of_month.empty?
      @day_of_month = Date.today.day.to_s
    end
  end

  def previous_menu
    go_to_menu(Cabinet.new(@user_id.to_i))
  end

  def successfully
    puts "You are well done."
    menu
  end
end
