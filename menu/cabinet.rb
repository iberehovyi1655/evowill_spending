class Cabinet < Menu
  def initialize(user_id)
    @user_id = user_id
    @menu = {
      "1" => { "text" => "1. Add spending.\n", "method_name" => "go_to_spendings" },
      "2" => { "text" => "2. Statistics.\n", "method_name" => "go_to_statistic" },
      "l" => { "text" => "l. Logout.", "method_name" => "previous_menu" },
      "x" => { "text" => "x. Exit.", "method_name" => "exit_app" }
    }
  end

  def menu(menu_hash = nil, menu_method_name = nil)
    puts "Cabinet:"
    super
  end

  private

  def go_to_spendings
    go_to_menu(Spending.new(@user_id))
  end

  def go_to_statistic
    go_to_menu(Statistic.new(@user_id))
  end

  def previous_menu
    go_to_menu(User.new)
  end
end
