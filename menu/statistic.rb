class Statistic < Menu
  def initialize(user_id)
    require 'date'
    @user_id = user_id
    @menu = {
      "1" => { "text" => "1. All categories statistic.\n", "method_name" => "all_statistic_menu" },
      "2" => { "text" => "2. Single category statistic.\n", "method_name" => "single_statistic_menu" },
      "3" => { "text" => "3. Reset all statistics.\n", "method_name" => "reset" },
      "p" => { "text" => "p. Previous menu.", "method_name" => "previous_menu" },
      "x" => { "text" => "x. Exit.", "method_name" => "exit_app" }
    }

    @date_statistic_menu = {
      "1" => { "text" => "1. Full statistic.\n", "method_name" => "full_statistic" },
      "2" => { "text" => "2. Yearly statistic.\n", "method_name" => "yearly_statistic" },
      "3" => { "text" => "3. Monthly statistic.\n", "method_name" => "monthly_statistic" },
      "4" => { "text" => "4. Daily statistic.\n", "method_name" => "daily_statistic" },
      "p" => { "text" => "p. Previous menu.", "method_name" => "menu" },
      "x" => { "text" => "x. Exit.", "method_name" => "exit_app" }
    }
    db_connect
  end

  def menu(menu_hash = nil, menu_method_name = nil, msg = nil, reset = true)
    if reset
      reset_params
    end
    puts msg ? msg : "Statistics menu:"
    super(menu_hash, menu_method_name)
  end

  private

  def reset_params
    @category_id = nil
    @year = ""
    @month = ""
    @day = ""
  end

  def all_statistic_menu
    menu(@date_statistic_menu, __method__, "All categories statistic menu:")
  end

  def single_statistic_menu
    show_categories
    set_category_id
    menu(@date_statistic_menu, __method__, "Single category statistic menu:", false)
  end

  def full_statistic
    get_statistic
  end

  def yearly_statistic
    set_year
    get_statistic
  end

  def set_year
    puts "Year:"
    begin
      @year = Date.parse("#{gets.chomp}-1-1").year.to_s
    rescue ArgumentError
      puts "Please insert valid year."
      set_year
    end
  end

  def monthly_statistic
    set_year
    set_month
    get_statistic
  end

  def set_month
    puts "Month:"
    begin
      @month = Date.parse("1-#{gets.chomp}-1").month.to_s
    rescue ArgumentError
      puts "Please insert valid month."
      set_month
    end
  end

  def daily_statistic
    set_year
    set_month
    set_day
    get_statistic
  end

  def set_day
    puts "Day:"
    begin
      @day = Date.parse("1-1-#{gets.chomp}").day.to_s
    rescue ArgumentError
      puts "Please insert valid day."
      set_day
    end
  end

  def get_statistic
    sql = "SELECT categories.name as name, SUM(spendings.price) as total_price FROM spendings
 LEFT JOIN categories ON spendings.category_id = categories.id
 WHERE user_id = #{@user_id} "
    sql += @category_id ? " AND category_id = #{@category_id} " : ""
    sql += !@year.empty? ? " AND YEAR(spent_at) = '#{@year}' " : ""
    sql += !@month.empty? ? " AND MONTH(spent_at) = '#{@month}' " : ""
    sql += !@day.empty? ? " AND DAY(spent_at) = '#{@day}' " : ""
    sql += "GROUP BY categories.id ORDER BY categories.name"
    statistic = connection.query(sql)
    puts "name - total_price"
    statistic.each do |name, total_price|
      puts "#{name} - #{total_price}"
    end
    menu
  end

  def reset
    puts "Are you sure? y/N"
    reset_statistic = gets.chomp
    if reset_statistic.downcase == "y"
      delete_user_spendings
      puts "All your spendings was deleted"
    end
    menu
  end

  def delete_user_spendings
    stmt = @connection.prepare("DELETE FROM spendings WHERE user_id = #{@user_id}")
    stmt.execute
  end

  def previous_menu
    go_to_menu(Cabinet.new(@user_id.to_i))
  end
end