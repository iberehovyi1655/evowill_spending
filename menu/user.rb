class User < Menu
  attr_reader :id

  def initialize
    require 'bcrypt'

    @menu = {
      "hello_text" => { "text" => "Hello guest!\nSelect menu item number:\n" },
      "1" => { "text" => "1. Login.\n", "method_name" => "sign_in" },
      "2" => { "text" => "2. Create account.\n", "method_name" => "sign_up" },
      "x" => { "text" => "x. Exit.", "method_name" => "exit_app" }
    }
    db_connect
  end
  def menu(menu_hash = nil, menu_method_name = nil)
    puts "Main menu:"
    super
  end

  private

  def sign_in
    login
    password
    auth
    go_to_cabinet
  end

  def sign_up
    login
    password
    save
    go_to_cabinet
  end

  def login
    puts "Login:"
    @name = gets.chomp
  end

  def password
    puts "Password:"
    @password = gets.chomp
  end

  def save
    begin
      stmt = connection.prepare('INSERT INTO users (login, password) VALUES (?,?)')
      @id = stmt.execute(@name, BCrypt::Password.create(@password)).insert_id
    rescue Mysql::ServerError::DupEntry
      puts "This login already exist. Please enter another name."
      menu
    end
  end

  def go_to_cabinet
    puts "Welcome, #{@name}!"
    go_to_menu(Cabinet.new(@id))
  end

  def auth
    user = connection.query("SELECT id, password as hashed_password FROM users WHERE login = '#{@name}'").fetch_hash
    unless user && user.key?("id") && BCrypt::Password.new(user['hashed_password']) == @password
      puts "Wrong login or password."
      menu
    end
    @id = user["id"]
  end
end
