class Menu
  include Database

  def menu(menu_hash = nil, menu_method_name = nil)
    m = menu_hash || @menu
    puts "------------------"
    m.each { |key, item| puts item["text"] }
    selected_item = gets.chomp
    unless m.has_key?(selected_item)
      puts "Item not exist. Please select number from existing items."
      menu_method_name ? send(menu_method_name) : menu
    end
    puts "------------------"
    send(m[selected_item]["method_name"])
  end

  protected

  def show_categories
    all_categories = connection.query("SELECT id, name FROM categories ORDER BY id")
    @categories_ids = []
    puts "Categories:"
    all_categories.each do |id, name|
      puts "#{id}. #{name}"
      @categories_ids.push(id.to_i)
    end
  end

  def set_category_id
    puts "Select id from existing categories table."
    @category_id = gets.chomp.to_i
    unless @categories_ids.include?(@category_id)
      puts "There is no such category, enter id from the list"
      set_category_id
    end
  end

  def go_to_menu(obj)
    obj.menu
  end

  def exit_app
    abort("Bye!")
  end
end
