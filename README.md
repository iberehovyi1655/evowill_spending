To create the application I used ruby (version **ruby 3.1.0**) and MySQL (ver **8.0.29-0ubuntu0.20.04.3**).

To start application create user with name **Ivan** and password **124** and database with name **spending**.
Or change username, password and database name in file db/database.rb method **db_connect**.

* 1. Then run next command in console:
```
ruby db/init.rb
```

* If db/init.rb completed successfully, you will see the following message:
![img_6.png](readme_images/img_6.png)

* 2. Then run in console:
```
ruby index.rb
```

```
Then you can see the main menu of application 
and navigate over application menus by changing the number of menu item: 
```
![img.png](readme_images/img.png)

```
 You can create a new user or login into "test user" with password "333".
```
![img_1.png](readme_images/img_1.png)

```
 You can add spendings to a new category or select a category from the list.
 Also, if you entered the wrong menu, you can always go back.
```
![img_2.png](readme_images/img_2.png)

```
 You can look statistics for all categories at once, or select one specific category 
 and view statistics for it.
 You also have the option to delete your statistics.
```
![img_3.png](readme_images/img_3.png)

```
 You can view full statistics for a specific period of time.
```
![img_4.png](readme_images/img_4.png)

```
 After adding your expenses, you will see a success message.
```
![img_5.png](readme_images/img_5.png)

```
 You can view full spending statistics for all your categories
```
![img.png](readme_images/img_7.png)

```
 You can always exit the console application by pressing "x" or "ctrl+c".
```